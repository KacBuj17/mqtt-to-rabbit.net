using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DotNetEnv;

namespace api.Configs
{
    public class AppConfig
    {
        public string MachineId{ get; set; }
        public string RabbitMQ_Host{ get; set;}
        public int RabbitMQ_Port { get; set; }
        public string RabbitMQ_ClientId { get; set; }
        public string RabbitMQ_ClientSecret { get; set; }
        public int  RabbitMQ_TokenLifespan { get; set; }
        public string Mqtt_Host { get; set; }

        public AppConfig()
        {
            Env.Load();

            MachineId = Environment.GetEnvironmentVariable("MachineId");

            RabbitMQ_Host = Environment.GetEnvironmentVariable("RabbitMQ_Host") ?? "localhost";
            RabbitMQ_Port = int.Parse(Environment.GetEnvironmentVariable("RabbitMQ_Port") ?? "5672");

            RabbitMQ_ClientId = Environment.GetEnvironmentVariable("RabbitMQ_ClientId");
            RabbitMQ_ClientSecret = Environment.GetEnvironmentVariable("RabbitMQ_ClientSecret");
            RabbitMQ_TokenLifespan = int.Parse(Environment.GetEnvironmentVariable("RabbitMQ_TokenLifespan") ?? "60");

            Mqtt_Host = Environment.GetEnvironmentVariable("Mqtt_Host") ?? "localhost";
        }
    }
}