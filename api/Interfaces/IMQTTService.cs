using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MQTTnet.Client;

namespace api.Interfaces
{
    public interface IMQTTService
    {
        public Task RunLoopAsync();
        public void SampleCallback(object sender, MqttApplicationMessageReceivedEventArgs e);
        
    }
}