using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace api.Interfaces
{
    public interface IRabbitMQService
    {
        public void Send(string value, string machineId, string measurmentId);
        public Task<string> NewAccessToken();
        public void UpdateAccessToken(double tokenLifespan);
        public void StartUpdateAccessTokenRoutine(float tokenLifespan);
    }
}