using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MQTTnet.Client;

namespace api.Interfaces
{
    public interface IMQTTCallback
    {
        public Task MqttCallback(MqttApplicationMessageReceivedEventArgs e);
    }
}