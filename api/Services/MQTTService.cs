using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using api.Configs;
using api.Helpers;
using api.Interfaces;
using MQTTnet;
using MQTTnet.Client;


namespace api.Services
{
    public class MQTTService : IMQTTService
    {
        private readonly AppConfig _appConfig;
        private readonly IRabbitMQService _rabbitMQService;
        private readonly IMQTTCallback _mqttCallback;
        
        public MQTTService(AppConfig appConfig, IRabbitMQService rabbitMQService, IMQTTCallback mqttCallback)
        {
            _appConfig = appConfig;
            _rabbitMQService = rabbitMQService;
            _mqttCallback = mqttCallback;
        }
        public async Task RunLoopAsync()
        {
            var factory = new MqttFactory();
            var mqttClient = factory.CreateMqttClient();

            mqttClient.ApplicationMessageReceivedAsync += HandleMqttMessageReceived;

            var options = new MqttClientOptionsBuilder()
                .WithTcpServer(_appConfig.Mqtt_Host)
                .Build();

            await mqttClient.ConnectAsync(options);
            await mqttClient.SubscribeAsync("#");
        }

        private Task HandleMqttMessageReceived(MqttApplicationMessageReceivedEventArgs eventArgs)
        {
            return _mqttCallback.MqttCallback(eventArgs);
        }

        public void SampleCallback(object sender, MqttApplicationMessageReceivedEventArgs e)
        {
            Console.WriteLine($"{e.ApplicationMessage.Topic} {System.Text.Encoding.UTF8.GetString(e.ApplicationMessage.PayloadSegment)}");
        }
    }
}