using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api.Configs;
using api.Interfaces;
using System.Threading;
using api.Helpers;
using RabbitMQ.Client;
using System.Reflection;
using System.Text;

namespace api.Services
{
    public class RabbitMQService : IRabbitMQService
    {
        private readonly AppConfig _appConfig;
        private readonly IConnection _connection;
        private readonly IModel _mainChannel;
        private readonly ITokenService _token;

        public RabbitMQService(AppConfig appConfig, ITokenService token)
        {
            _appConfig = appConfig;
            _token = token;

            var tokenRequest = NewAccessToken();
            tokenRequest.Wait();
            var password = tokenRequest.Result;

            var factory = new ConnectionFactory
            {
                UserName = "",
                Password = password, 
                HostName = appConfig.RabbitMQ_Host,
                Port = appConfig.RabbitMQ_Port,
                VirtualHost = "/"
            };

        _connection = factory.CreateConnection();
        _mainChannel = _connection.CreateModel();
        }
        
        public async Task<string> NewAccessToken()
        {
            try
            {
                var accesToken = await _token.GetAccessToken(_appConfig.RabbitMQ_ClientId, _appConfig.RabbitMQ_ClientSecret);
                return accesToken;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Cannot read token: " + ex.ToString());
                return "null";
            }
        }

        public void Send(string value, string machineId, string measurmentId)
        {
            var properties = _mainChannel.CreateBasicProperties();
            properties.ContentType = "double";
            properties.Headers = new Dictionary<string, object>
            {
                { "mach", machineId },
                { "meas", measurmentId }
            };

            _mainChannel.BasicPublish(
                exchange: "",
                routingKey: "test",
                basicProperties: properties,
                body: Encoding.UTF8.GetBytes(value)
            );
        }

        public async void UpdateAccessToken(double tokenLifespan)
        {
            while (true)
            {
                Thread.Sleep(TimeSpan.FromSeconds(tokenLifespan));
                var newToken = await NewAccessToken();
                _connection.UpdateSecret(newToken, "secret");
                Console.WriteLine("Updated JWT");
            }
        }

        public void StartUpdateAccessTokenRoutine(float tokenLifespan)
        {
            Thread thread = new Thread(() => UpdateAccessToken(tokenLifespan));
            thread.Start();
        }
    }
}