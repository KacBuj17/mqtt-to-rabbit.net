using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Http;
using System.Text.Json;
using api.Interfaces;

namespace api.Helpers
{
    public class TokenService : ITokenService
    {
        private readonly HttpClient _httpClient;

        public TokenService()
        {
            _httpClient = new HttpClient();
        }

        public async Task<string> GetAccessToken(string clientId, string clientSecret)
        {
            var requestData = new Dictionary<string, string>
            {
                { "client_id", clientId },
                { "client_secret", clientSecret },
                { "grant_type", "client_credentials" }
            };

            var requestContent = new FormUrlEncodedContent(requestData);

            var response = await _httpClient.PostAsync("https://iam.emotoagh.eu.org/realms/app/protocol/openid-connect/token", requestContent);

            response.EnsureSuccessStatusCode();

            var responseBody = await response.Content.ReadAsStringAsync();

            dynamic tokenObject = JsonSerializer.Deserialize<dynamic>(responseBody);

            string accessToken = tokenObject["access_token"];

            return accessToken;
        }
    }
}