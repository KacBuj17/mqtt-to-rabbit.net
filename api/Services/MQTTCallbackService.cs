using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using api.Services;
using RabbitMQ.Client;
using MQTTnet;
using MQTTnet.Client;
using api.Interfaces;

namespace api.Helpers
{
    public class MQTTCallbackService : IMQTTCallback
    {
        private readonly IRabbitMQService _rabbitMQ;
        private readonly string _machineId;

        public MQTTCallbackService(IRabbitMQService rabbitMQ, string machineId)
        {
            _rabbitMQ = rabbitMQ;
            _machineId = machineId;
        }

        public Task MqttCallback(MqttApplicationMessageReceivedEventArgs e)
        {
            string topic = e.ApplicationMessage.Topic;
            string payload = Encoding.UTF8.GetString(e.ApplicationMessage.PayloadSegment);

            Console.WriteLine($"{topic} {payload}");
            _rabbitMQ.Send(payload, _machineId, topic);

            return Task.CompletedTask;
        }
    }
}