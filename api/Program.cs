﻿using System.Runtime.CompilerServices;
using api.Configs;
using api.Helpers;
using api.Interfaces;
using api.Services;

internal class Program
{
    static async Task Main(string[] args)
    {
        AppConfig conf = new AppConfig();
        TokenService token = new TokenService();

        RabbitMQService rabbit = new RabbitMQService(conf, token);
        rabbit.StartUpdateAccessTokenRoutine(conf.RabbitMQ_TokenLifespan - 5);

        MQTTCallbackService mQTTCallback= new MQTTCallbackService(rabbit, conf.MachineId);
        MQTTService mqtt = new MQTTService(conf, rabbit, mQTTCallback);

        await mqtt.RunLoopAsync();
    }
}